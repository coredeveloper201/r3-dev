<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settings extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','value','user_id'];

    protected $hidden = ['created_at','updated_at'];
    protected $guarded = [];
}
