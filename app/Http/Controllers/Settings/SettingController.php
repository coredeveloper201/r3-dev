<?php

namespace App\Http\Controllers\Settings;

use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $model = Settings::orderBy('id', 'desc')->get();
        return response()->json([
            'record' => $model
            ],200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = Settings::create($request->only(['name','value','user_id']));
        return response()->json([
            'record' => $model
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Settings::findOrFail($id);
        return response()->json([
            'record' => $model
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Settings::findOrFail($id);
        $model->update($request->only(['name','value','user_id']));
        return response()->json([
            'record' => $model
        ],200);
    }
}
